# README #

This is a Task Management web application that employs the usual CRUDS operations (minus delete) to create and manage your tasks. It was built under the Laravel platform. 

First, set up the environment file by creating a file called "local/.env". The important thing is to set up the database configuration options:

```
DB_HOST=localhost
DB_DATABASE=task
DB_USERNAME=root
DB_PASSWORD=
```

If you change any of the above options, make sure that the database, username, and password are all valid for your configuration.

Then, to initialize, change into the "local" directory where the "artisan" script is located. Run the following PHP commands to initialize the table structures and seed the database with initial data:

```
$ php artisan migrate
$ php db:seed
```

Set up an .htaccess file (example as follows):

```
<IfModule mod_rewrite.c>
    <IfModule mod_negotiation.c>
        Options -MultiViews
    </IfModule>

    RewriteEngine On

    # Redirect Trailing Slashes If Not A Folder...
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteRule ^(.*)/$ /$1 [L,R=301]

    # Handle Front Controller...
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^(.*)$ /index.php/$1 [L]
</IfModule>
```

If setting up under a dedicated domain, the URL to access the project is as follows:

http://localhost/task

If all goes well, you should now be able to edit and modify your tasks as needed!

There are the source code files I either modified or created to make this project:

```
 css/app.css
 js/app.js
 local/app/Http/Controllers/TaskController.php
 local/app/Http/Controllers/TaskResourceController.php
 local/app/Http/routes.php
 local/app/Managers/StateManager.php
 local/app/Managers/TaskListManager.php
 local/app/Managers/TaskManager.php
 local/app/Mappers/StateMapper.php
 local/app/Mappers/TaskListMapper.php
 local/app/Mappers/TaskMapper.php
 local/app/Objects/StateObject.php
 local/app/Objects/TaskListObject.php
 local/app/Objects/TaskObject.php
 local/app/Objects/TaskToListObject.php
 local/app/Objects/UserObject.php
 local/app/State.php
 local/app/Task.php
 local/app/TaskList.php
 local/app/TaskToList.php
 local/database/migrations/2015_12_21_221636_create_lists_table.php
 local/database/migrations/2015_12_21_222907_create_states_table.php
 local/database/migrations/2015_12_21_223523_create_tasks_table.php
 local/database/migrations/2015_12_21_223906_create_tasks_to_lists_table.php
 local/database/seeds/DatabaseSeeder.php
 local/database/seeds/StateTableSeeder.php
 local/database/seeds/TaskListTableSeeder.php
 local/database/seeds/TaskTableSeeder.php
 local/database/seeds/UserTableSeeder.php
 local/resources/views/index.blade.php
 local/resources/views/layouts/app.blade.php
```

Finally, these are the items for the project that were successfully completed:

* As a user, I should be able to create a task.
* As a user, I should be able to describe that task.
* As a user, I should be able to assign a state to the task (for instance, 'Open' and 'Completed' or 'To do,' 'Doing', and 'Done')
* As a user, I should be able to CHANGE the state of the task.
* As a developer, I should be able to read documentation about this project.
* As a user, I should be able to display any information related to tasks (and lists, if applicable).
* As a user, I should be able to assign tasks to a list.
* As a user, I should be able to assign tasks to MULTIPLE lists.
* As a user, I should have a basic frontend interface for this project.
* As a developer, I should be able to extend this project through RESTful API endpoints.

These were the items that were not completed:

* As a user, I should be able to delete a task.
* As a user, I should be able to create a list that can house tasks.
* As a user, I should be able to generate a tweet every time I complete a task.
* As a user, I should be able to use twitter to change the state of a task.
* As a developer, I should be able to automatically generate API Endpoint documentation for this project when I modify it.