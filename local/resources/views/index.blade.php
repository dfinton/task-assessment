@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row padded">
        <div class="col-lg-3"></div>
        <div class="col-lg-6 task-list centered" data-task-list-url="{{ url('/rest/task') }}">
            <i class="fa fa-refresh fa-spin fa-5x"></i>
        </div>
        <div class="col-lg-3"></div>
    </div>
    <div class="row padded">
        <div class="col-lg-3"></div>
        <div class="col-lg-6"><a class="create-task" href="{{ url('/rest/task') }}">Create</a></div>
        <div class="col-lg-3"></div>
    </div>
</div>

<!-- View Modal -->
<div class="modal fade" id="view-task-modal" tabindex="-1" role="dialog" aria-labelledby="view-task-modal-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                <h4 class="modal-title" id="view-task-modal-label"></h4>
            </div>
            <div class="modal-body" id="view-task-modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Edit Modal -->
<div class="modal fade" id="edit-task-modal" tabindex="-1" role="dialog" aria-labelledby="edit-task-modal-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                <h4 class="modal-title" id="edit-task-modal-label"></h4>
            </div>
            <div class="modal-body" id="edit-task-modal-body">
                <form id="edit-task-form">
                    <div class="form-submit padded">
                        <label for="task-name" class="control-label">Name</label>
                        <input type="text" class="form-control" id="task-name" name="task-name"></input>
                    </div>
                    <div class="form-submit padded">
                        <div class="btn-group" data-toggle="buttons" id="task-state-group"></div>
                    </div>
                    <div class="form-submit padded">
                        <label for="task-list" class="control-label">Lists (Hold Shift or Control Key to Select Multiple Lists)</label>
                        <select id="task-list" name="task-list" class="form-control" multiple="multiple">
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>
                        </select>
                    </div>
                    <div class="form-submit padded">
                        <label for="task-description" class="control-label">Description</label>
                        <textarea rows="5" class="form-control" id="task-description" name="task-description"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <span id="save-task-button"></span>
            </div>
        </div>
    </div>
</div>
@endsection
