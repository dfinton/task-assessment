<?php

use Illuminate\Database\Seeder;

require __DIR__ . '/StateTableSeeder.php';
require __DIR__ . '/TaskListTableSeeder.php';
require __DIR__ . '/TaskTableSeeder.php';
require __DIR__ . '/UserTableSeeder.php';

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->call(UserTableSeeder::class);
        $this->call(StateTableSeeder::class);
        $this->call(TaskListTableSeeder::class);
        $this->call(TaskTableSeeder::class);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
