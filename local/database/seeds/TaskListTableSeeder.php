<?php

use Illuminate\Database\Seeder;
use App\TaskList;
use App\User;

class TaskListTableSeeder extends Seeder {
    /**
     * Create a few sample lists
     *
     * @return void
     */
    public function run()
    {
        DB::table('lists')->delete();

        $user = User::where('email', 'john.doe@test.com')
            ->first();

        $userId = $user->id;

        TaskList::create(array(
            'name' => 'All The Things',
            'user_id' => $userId,
        ));

        TaskList::create(array(
            'name' => 'Needful Things',
            'user_id' => $userId,
        ));

        TaskList::create(array(
            'name' => 'Bad Stephen King Novels',
            'user_id' => $userId,
        ));

        TaskList::create(array(
            'name' => 'Only After I\'ve Had My Coffee',
            'user_id' => $userId,
        ));

        TaskList::create(array(
            'name' => 'When I Rule The World',
            'user_id' => $userId,
        ));
    }
}
