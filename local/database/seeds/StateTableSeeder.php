<?php

use Illuminate\Database\Seeder;
use App\State;

class StateTableSeeder extends Seeder {
    /**
     * Create a few task states
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->delete();

        State::create(array(
            'name' => 'Open',
        ));

        State::create(array(
            'name' => 'Completed',
        ));

        State::create(array(
            'name' => 'In Progress',
        ));

        State::create(array(
            'name' => 'Cancelled',
        ));

        State::create(array(
            'name' => 'Reopened',
        ));
    }
}
