<?php

use Illuminate\Database\Seeder;

use App\State;
use App\Task;
use App\TaskList;
use App\TaskToList;
use App\User;

class TaskTableSeeder extends Seeder {
    /**
     * Create some tasks and associate them with a user, a state, and one or more lists
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks_to_lists')->delete();
        DB::table('tasks')->delete();

        $user = User::where('email', 'john.doe@test.com')
            ->first();

        $state = State::where('name', 'In Progress')
            ->first();

        $firstTaskList = TaskList::where('name', 'When I Rule The World')
            ->first();

        $secondTaskList = TaskList::where('name', 'Bad Stephen King Novels')
            ->first();

        $task = Task::create(array(
            'name' => 'Conquer the world',
            'description' => 'Pinky keeps asking me that question every night',
            'user_id' => $user->id,
            'state_id' => $state->id,
        ));

        TaskToList::create(array(
            'task_id' => $task->id,
            'list_id' => $firstTaskList->id,
        ));

        TaskToList::create(array(
            'task_id' => $task->id,
            'list_id' => $secondTaskList->id,
        ));
    }
}
