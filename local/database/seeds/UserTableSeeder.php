<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder {
    /**
     * Create a few users
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        User::create(array(
            'name' => 'John Doe',
            'email' => 'john.doe@test.com',
        ));

        User::create(array(
            'name' => 'Mary Sue',
            'email' => 'mary.sue@test.com',
        ));
    }
}
