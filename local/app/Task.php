<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\State;
use App\TaskList;
use App\User;

class Task extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s';

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function state() {
        return $this->belongsTo(State::class);
    }

    public function taskLists() {
        return $this->belongsToMany(TaskList::class, 'tasks_to_lists', 'task_id', 'list_id');
    }
}
