<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'TaskController@index');

/*
| --------------------------------------------------------------------------
| RESTful API
| --------------------------------------------------------------------------
|
| Laravel refers to RESTful endpoints as "resource controllers", so
| we will use them as the basis for the back-end of the application
|
*/

Route::resource('rest/task', 'TaskResourceController');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
