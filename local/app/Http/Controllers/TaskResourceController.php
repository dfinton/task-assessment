<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Managers\TaskManager;
use App\Managers\TaskListManager;
use App\Managers\StateManager;
use App\User;
use Auth;

class TaskResourceController extends Controller
{
    /**
     * Where our private manager will be stored
     *
     * @var App\Managers\TaskManager $taskManager
     */
    private $taskManager;

    /**
     * Where our private manager will be stored
     *
     * @var App\Managers\TaskListManager $taskListManager
     */
    private $taskListManager;
    /**
     * Where our private manager will be stored
     *
     * @var App\Managers\StateManager $stateManager
     */
    private $stateManager;

    /**
     * Contructor for the TaskResourceController class
     *
     * @param App\Managers\TaskManager $taskManager
     * @param App\Managers\TaskListManager $taskListManager
     * @param App\Managers\StateManager $stateManager
     */
    public function __construct(TaskManager $taskManager, TaskListManager $taskListManager, StateManager $stateManager)
    {
        // Fakes a middleware call where the application pretends that the
        // "John Doe" user is actually logged in. TODO: Fix this
        $user = User::where('email', 'john.doe@test.com')
            ->first();

        Auth::login($user);

        // Assign dependencies to our private variables
        $this->taskManager = $taskManager;
        $this->taskListManager = $taskListManager;
        $this->stateManager = $stateManager;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Illuminate\Http\Request  $request
     * @return Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();

        // Gather our list of tasks
        $taskArrayData = $this->taskManager->getTaskArrayDataByUserId($user->id);

        // Gather our states for the form
        $stateArrayData = $this->stateManager->getStateArrayData();

        // Gather our list of lists for the form
        $taskListArrayData = $this->taskListManager->getTaskListArrayData();

        // Put our data intot he final payload and send it back to the requestor
        $payload = array(
            'taskArray' => $taskArrayData,
            'stateArray' => $stateArrayData,
            'taskListArray' => $taskListArrayData,
        );

        return response()->json($payload);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  Illuminate\Http\Request  $request
     * @return Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // automatically created by artisan but not defined
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Illuminate\Http\Request  $request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $request->user();
        $taskInput = $request->all();

        $taskData = $this->taskManager->storeTask($user->id, $taskInput);

        return $taskData;
    }

    /**
     * Display the specified resource.
     *
     * @param  Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Illuminate\Http\Response
     */
    public function show(Request $request, $taskId)
    {
        $user = $request->user();

        $taskData = $this->taskManager->getTaskDataByUserId($user->id, $taskId);

        return response()->json($taskData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Illuminate\Http\Response
     */
    public function edit($id)
    {
        // automatically created by artisan but not defined
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Illuminate\Http\Response
     */
    public function update(Request $request, $taskId)
    {
        $user = $request->user();
        $taskInput = $request->all();

        $taskData = $this->taskManager->storeTask($user->id, $taskInput, $taskId);

        return $taskData;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // automatically created by artisan but not defined
    }
}
