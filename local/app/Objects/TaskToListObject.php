<?php

namespace App\Objects;

use App\TaskToList;

class TaskToListObject {
    /**
     * Clear existing task to task list associations and create the new ones
     *
     * @param App\Task $task
     * @param array $taskInput
     * @return boolean
     */
    public function setTaskToListAssoc($task, $taskInput)
    {
        $taskId = $task->id;
        $taskListIds = array();

        if (isset($taskInput['listIdArray'])) {
            $taskListIds = $taskInput['listIdArray'];
        }

        // Clear existing associations
        TaskToList::where('task_id', $taskId)
            ->delete();

        // Add the new task to list associations
        foreach ($taskListIds as $taskListId) {
            $taskToList = new TaskToList;

            $taskToList->task_id = $taskId;
            $taskToList->list_id = $taskListId;

            $taskToList->save();
        }

        // We made it this far; report our success
        return true;
    }
}
