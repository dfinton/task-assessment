<?php

namespace App\Objects;

use App\Task;
use App\Mappers\TaskMapper;

class TaskObject {
    /**
     * Our taskMapper vairable to map the object to an array
     *
     * @var TaskMapper $taskMapper
     */
    private $taskMapper;

    /**
     * Imports our dependency injections
     *
     * @param App\Mappers\TaskMapper
     */
    public function __construct(TaskMapper $taskMapper)
    {
        $this->taskMapper = $taskMapper;
    }

    /**
     * Responsible for retrieving a Task based on ID or null if the ID is valid.
     * If the provided ID is null or not provided at all, a new Task object is
     * returned instead
     *
     * @param int $userId
     * @param int $taskId | null
     * @return App\Task | null
     */
    public function getTask($userId, $taskId = null)
    {
        $task = null;

        // If a task ID is defined, grab it from the DB
        if (null !== $taskId) {
            $task = Task::where('user_id', $userId)
                ->find($taskId);
        }

        // If the task is not found, we return a null and stop immediately
        if (null !== $taskId && null === $task) {
            return null;
        }

        // If we got this far, create our new task object if it's still null
        if (null === $task) {
            $task = new Task;
        }

        return $task;
    }

    /** 
     * Responsible for saving a task while doing sanity checking for the
     * various parameters. If everything looks good, return the saved
     * task; otherwise return null
     *
     * TODO: Do something that actually checks for input validation
     *
     * @param App\Task $task
     * @param int $userId
     * @param array $taskData
     * @return App\Task | null
     */
    public function saveTask($task, $userId, $taskInput)
    {
        // set our fields
        $task->name = $taskInput['name'];
        $task->description = $taskInput['description'];
        $task->user_id = $userId;
        $task->state_id = $taskInput['stateId'];

        // save our data to the DB
        $task->save();

        // book it! done!
        return $task;
    }

    /**
     * Get a list of tasks based on user ID
     *
     * @param int $userId
     * @return array
     */
    public function getTaskArrayDataByUserId($userId) 
    {
        // Gather our list of tasks
        $taskArray = Task::where('user_id', $userId)
            ->get();

        $taskArrayData = $this->taskMapper->taskArrayToData($taskArray);

        return $taskArrayData;
    }

    /**
     * Get a task based on user ID and task ID
     *
     * @param int $userId
     * @param int $taskId
     * @return App/Task | null
     */
    public function getTaskDataByUserId($userId, $taskId) 
    {
        // Gather our list of tasks
        $task = Task::where('user_id', $userId)
            ->find($taskId);

        if (null === $task) {
            return null;
        }

        $taskData = $this->taskMapper->taskToData($task);

        return $taskData;
    }

    /**
     * Convert a Task object to array data using the mapper object
     *
     * @param App\Task $task
     * @return array
     */
    public function taskToData (Task $task) {
        $taskData = $this->taskMapper->taskToData($task);

        return $taskData;
    }
}
