<?php

namespace App\Objects;

use App\Mappers\TaskListMapper;
use App\TaskList;

class TaskListObject {
    /**
     * Our TaskListMapper vairable to map the object to an array
     *
     * @var TaskListMapper $taskListMapper
     */
    private $taskListMapper;

    /**
     * Imports our dependency injections
     *
     * @param App\Mappers\TaskListMapper $taskListMapper
     */
    public function __construct(TaskListMapper $taskListMapper)
    {
        $this->taskListMapper = $taskListMapper;
    }

    /**
     * Gets the list of task states
     *
     * @return array
     */
    public function getTaskListArrayData() {
        $taskListArray = TaskList::get();
        $taskListArrayData = $this->taskListMapper->taskListArrayToData($taskListArray);

        return $taskListArrayData;
    }
}
