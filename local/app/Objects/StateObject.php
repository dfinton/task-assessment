<?php

namespace App\Objects;

use App\Mappers\StateMapper;
use App\State;

class StateObject {
    /**
     * Our stateMapper vairable to map the object to an array
     *
     * @var StateMapper $stateMapper
     */
    private $stateMapper;

    /**
     * Imports our dependency injections
     *
     * @param App\Mappers\StateMapper $stateMapper
     */
    public function __construct(StateMapper $stateMapper)
    {
        $this->stateMapper = $stateMapper;
    }

    /**
     * Gets the list of task states
     *
     * @return array
     */
    public function getStateArrayData() {
        $stateArray = State::get();
        $stateArrayData = $this->stateMapper->stateArrayToData($stateArray);

        return $stateArrayData;
    }
}
