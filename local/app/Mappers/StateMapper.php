<?php

namespace App\Mappers;

class StateMapper {
    /**
     * Takes a list of state models and converts them into plain data
     *
     * @access public
     * @param array $stateList
     * @return array
     */
    public function stateArrayToData($stateList) 
    {
        $stateData = array();

        foreach ($stateList as $state) {
            $stateData[] = array(
                'id' => $state->id,
                'name' => $state->name,
            );
        }

        return $stateData;
    }

}
