<?php

namespace App\Mappers;

class TaskListMapper {
    /**
     * Takes a list of task list array models and converts them into plain data
     *
     * @access public
     * @param array $taskListArray
     * @return array
     */
    public function taskListArrayToData($taskListArray) 
    {
        $taskListArrayData = array();

        foreach ($taskListArray as $taskList) {
            $taskListArrayData[] = array(
                'id' => $taskList->id,
                'name' => $taskList->name,
            );
        }

        return $taskListArrayData;
    }
}
