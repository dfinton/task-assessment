<?php

namespace App\Mappers;

use App\Task;

class TaskMapper {
    /**
     * Takes a task model or null and converts it into plain data
     *
     * @access public
     * @param App\Task $task
     * @return array
     */
    public function taskToData(Task $task) 
    {
        if (null === $task) {
            return array();
        }

        $taskListData = array();
        $taskListArray = $task->taskLists;

        foreach ($taskListArray as $taskList) {
            $taskListData[] = array(
                'id' => $taskList->id,
                'name' => $taskList->name,
            );
        }

        $taskData = array(
            'id' => $task->id,
            'name' => $task->name,
            'description' => $task->description,
            'user' => array(
                'id' => $task->user->id,
                'name' => $task->user->name,
            ),
            'state' => array(
                'id' => $task->state->id,
                'name' => $task->state->name,
            ),
            'taskListArray' => $taskListData,
            'createdAt' => $task->created_at->toDateTimeString(),
            'updatedAt' => $task->updated_at->toDateTimeString(),
        );

        return $taskData;
    }

    /**
     * Takes a list of task models and converts them into plain data
     *
     * @access public
     * @param array $taskList
     * @return array
     */
    public function taskArrayToData($taskList) 
    {
        $taskListData = array();

        foreach ($taskList as $task) {
            $taskData = $this->taskToData($task);

            $taskListData[] = $taskData;
        }

        return $taskListData;
    }
}
