<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskToList extends Model
{
    public $timestamps = false;
    protected $table = 'tasks_to_lists';
    //
}
