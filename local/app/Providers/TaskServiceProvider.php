<?php

namespace App\Providers;

use App\Managers\TaskManager;
use App\Managers\TaskListManager;
use App\Managers\StateManager;
use App\Mappers\StateMapper;
use App\Mappers\TaskMapper;
use App\Mappers\TaskListMapper;
use App\Objects\TaskObject;
use App\Objects\TaskListObject;
use App\Objects\TaskToListObject;
use App\Objects\StateObject;
use Illuminate\Support\ServiceProvider;

class TaskServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // automatically created by artisan but not defined
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // The Objects
        $this->app->singleton(TaskObject::class, function($app) {
            return new TaskObject($app[TaskMapper::class]);
        });

        $this->app->singleton(TaskListObject::class, function($app) {
            return new TaskListObject($app[TaskListMapper::class]);
        });

        $this->app->singleton(TaskToListObject::class, function($app) {
            return new TaskToListObject();
        });

        $this->app->singleton(StateObject::class, function($app) {
            return new StateObject($app[StateMapper::class]);
        });

        // The managers
        $this->app->singleton(TaskManager::class, function($app) {
            return new TaskManager($app[TaskObject::class], $app[TaskToListObject::class]);
        });

        $this->app->singleton(TaskListManager::class, function($app) {
            return new TaskListManager($app[TaskListObject::class]);
        });

        $this->app->singleton(StateManager::class, function($app) {
            return new StateManager($app[StateObject::class]);
        });

        // The mappers
        $this->app->singleton(TaskMapper::class, function($app) {
            return new TaskMapper();
        });

        $this->app->singleton(TaskListMapper::class, function($app) {
            return new TaskListMapper($app[TaskMapper::class]);
        });

        $this->app->singleton(StateMapper::class, function($app) {
            return new StateMapper();
        });

    }
}
