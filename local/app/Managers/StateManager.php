<?php

namespace App\Managers;

use App\Objects\StateObject;

class StateManager {
    /**
     * Where our private state object will be stored
     *
     * @access private
     * @var App\Objects\StateObject $stateObject
     */
    private $stateObject;

    /**
     * Constructor for the class TaskManager
     *
     * @access public
     * @param App\Objects\StateObject $stateObject
     */
    public function __construct(StateObject $stateObject)
    {
        $this->stateObject = $stateObject;
    }

    /**
     * Gets the list of task states
     *
     * @return array
     */
    public function getStateArrayData()
    {
        $stateArray = $this->stateObject->getStateArrayData();

        return $stateArray;
    }
}
