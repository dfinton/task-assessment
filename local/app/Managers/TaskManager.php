<?php

namespace App\Managers;

use App\Objects\TaskObject;
use App\Objects\TaskToListObject;

class TaskManager {
    /**
     * Where our private task object will be stored
     *
     * @access private
     * @var App\Objects\TaskObject $taskObject
     */
    private $taskObject;

    /**
     * Where our private task object will be stored
     *
     * @access private
     * @var App\Objects\TaskToListObject $taskToListObject
     */
    private $taskToListObject;

    /**
     * Constructor for the class TaskManager
     *
     * @access public
     * @param App\Objects\TaskObject $taskObject
     * @param App\Objects\TaskToListObject $taskToListObject
     */
    public function __construct(TaskObject $taskObject, TaskToListObject $taskToListObject)
    {
        $this->taskObject = $taskObject;
        $this->taskToListObject = $taskToListObject;
    }

    /**
     * Get a list of tasks based on user ID
     *
     * @param int $userId
     * @return array
     */
    public function getTaskArrayDataByUserId($userId) 
    {
        // Gather our list of tasks
        $taskArrayData = $this->taskObject->getTaskArrayDataByUserId($userId);

        return $taskArrayData;
    }

    /**
     * Get a list of tasks based on user ID
     *
     * @param int $userId
     * @param int $taskId
     * @return array
     */
    public function getTaskDataByUserId($userId, $taskId) 
    {
        // Gather our list of tasks
        $task = $this->taskObject->getTaskDataByUserId($userId, $taskId);

        return $task;
    }

    /**
     * Responsible for inserting a new task or updating an existing task
     *
     * @param int $userId
     * @param array $taskInput
     * @param int $taskId | null
     * @return App\Task | null
     */
    public function storeTask($userId, $taskInput, $taskId = null) 
    {
        // Get the task or create a new one by calling our getTask method
        $task = $this->taskObject->getTask($userId, $taskId);

        if (null === $task) {
            return null;
        }

        // The task's properties are now set and we can store it now as a new
        // or modified object
        $task = $this->taskObject->saveTask($task, $userId, $taskInput);

        // reset the task to list relationships
        $this->taskToListObject->setTaskToListAssoc($task, $taskInput);

        // Convert our task object to array data for the controller
        $taskData = $this->taskObject->taskToData($task);

        // return back to the caller
        return $taskData;
    }
}

