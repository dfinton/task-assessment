<?php

namespace App\Managers;

use App\Objects\TaskListObject;

class TaskListManager {
    /**
     * Where our private taskList object will be stored
     *
     * @access private
     * @var App\Objects\TaskListObject $taskListObject
     */
    private $taskListObject;

    /**
     * Constructor for the class TaskManager
     *
     * @access public
     * @param App\Objects\TaskListObject $taskListObject
     */
    public function __construct(TaskListObject $taskListObject)
    {
        $this->taskListObject = $taskListObject;
    }

    /**
     * Gets the list of task taskLists
     *
     * @return array
     */
    public function getTaskListArrayData()
    {
        $taskListList = $this->taskListObject->getTaskListArrayData();

        return $taskListList;
    }
}
