var taskListUrl = $('div.task-list').data('task-list-url');

var editFormSubmit = function(event) {
    event.preventDefault();
};

var postTask = function(data) {
    $('#edit-task-modal').modal('hide');

    $.getJSON(taskListUrl, parseTaskListData);
};

var saveTask = function(event) {
    var httpVerb = $(this).attr('data-save-task-type');
    var taskUrl = $(this).attr('data-save-task-url');
    var taskListArray = [];

    $.each($('.task-list:selected'), function(index, taskListOption) {
        console.log($(taskListOption).val());
        taskListArray.push($(taskListOption).val());
    });

    var postData = {
        name : $('#task-name').val(),
        description : $('#task-description').val(),
        stateId : $('.task-state:checked').val(),
        listIdArray : taskListArray
    };

    var ajaxData = {
        type : httpVerb,
        url : taskUrl,
        data : postData,
        success : postTask
    };

    $.ajax(ajaxData);
};

var parseViewTaskData = function(data) {
    $('#view-task-modal-label').empty();
    $('#view-task-modal-label').html(data.name);
    $('#view-task-modal-body').empty();

    var stateLabel = $('<span>').addClass('bold').html('Status: ');
    var stateDiv = $('<div>').addClass('padded').html(stateLabel).append(data.state.name);
    var taskListLabel = $('<span>').addClass('bold').html('Member of Lists: ');
    var taskListDiv = $('<div>').addClass('padded').html(taskListLabel);
    var descriptionDiv = $('<div>').addClass('padded').html(data.description);

    for (var index = 0, len = data.taskListArray.length; index < len; index++) {
        var taskList = data.taskListArray[index];

        taskListDiv.append(taskList.name);

        if (index < len - 1) {
            taskListDiv.append(', ');
        }
    }

    $('#view-task-modal-body').append(stateDiv);
    $('#view-task-modal-body').append(taskListDiv);
    $('#view-task-modal-body').append(descriptionDiv);
};

var parseEditTaskData = function(data) {
    var taskStateButtonId = '#task-state-' + data.state.id;
    var taskStateLabelId = '#task-state-label-' + data.state.id;

    $('#edit-task-modal-label').empty();
    $('#edit-task-modal-label').html('Edit Task');

    // Set the text inputs
    $('#task-name').val(data.name);
    $('#task-description').html(data.description);
    
    // clear and set the status
    $('.task-state').removeAttr('checked');
    $('.task-state-label').removeClass('active');

    $(taskStateButtonId).attr('checked', 'checked');
    $(taskStateLabelId).addClass('active');

    // clear and set the lists
    $('.task-list').removeAttr('selected');

    for (var index = 0, len = data.taskListArray.length; index < len; index++) {
        var taskList = data.taskListArray[index];
        var taskListOptionId = '#task-list-' + taskList.id;

        $(taskListOptionId).attr('selected', 'selected');
    }
};

var clearEditTaskData = function(data) {
    $('#task-name').val('');
    $('#task-description').html('');
    $('.task-state').removeAttr('checked', false);
    $('.task-state-label').removeClass('active');
    $('.task-list').removeAttr('selected');

    $('.task-state:first').attr('checked', 'checked');
    $('.task-state-label:first').addClass('active');
};

var viewTask = function(event) {
    event.preventDefault();

    var taskLink = $(this).attr('href');
    var spinnerDiv = $('<div>').addClass('full-width centered');

    spinnerDiv.html($('<i>').addClass('fa fa-refresh fa-spin fa-3x'));

    $('#view-task-modal-label').empty();
    $('#view-task-modal-label').html('Loading...');
    $('#view-task-modal-body').empty();
    $('#view-task-modal-body').html(spinnerDiv);
    $('#view-task-modal').modal('show');

    $.getJSON(taskLink, parseViewTaskData);
};

var editTask = function(event) {
    event.preventDefault();

    var taskLink = $(this).attr('href');
    var saveButton = $('<button>').html('Save Changes')
        .addClass('btn btn-primary')
        .attr('type', 'button')
        .attr('data-save-task-type', 'put')
        .attr('data-save-task-url', taskLink);

    $('#edit-task-modal-label').empty();
    $('#edit-task-modal-label').html('Loading...');
    $('#save-task-button').empty();
    $('#save-task-button').html(saveButton);
    $('#edit-task-modal').modal('show');

    saveButton.click(saveTask);
    $.getJSON(taskLink, parseEditTaskData);
};

var createTask = function(event) {
    event.preventDefault();

    var taskLink = $(this).attr('href');
    var saveButton = $('<button>').html('Save Changes')
        .addClass('btn btn-primary')
        .attr('type', 'button')
        .attr('data-save-task-type', 'post')
        .attr('data-save-task-url', taskLink);

    $('#edit-task-modal-label').empty();
    $('#edit-task-modal-label').html('Create Task');
    $('#save-task-button').empty();
    $('#save-task-button').html(saveButton);
    $('#edit-task-modal').modal('show');
    
    saveButton.click(saveTask);
    clearEditTaskData();
};

var parseTaskListData = function(data) {
    var taskListDiv = $('div.task-list');

    taskListDiv.empty();

    var table = $('<table>').addClass('table-striped full-width');
    var tableHeader = $('<thead>');
    var tableBody = $('<tbody>') ;
    var topRow = $('<tr>');

    topRow.append($('<th>').html('Status'));
    topRow.append($('<th>').html('Name'));
    topRow.append($('<th>'));

    // populate the main list of tasks
    for (var index = 0, len = data.taskArray.length; index < len; index++) {
        var task = data.taskArray[index];
        var dataRow = $('<tr>');
        var statusField =  $('<td>').addClass('left-justified').html(task.state.name);
        var taskUrl = taskListUrl + '/' + task.id;
        var viewLink = $('<a>').addClass('view-task').attr('href', taskUrl).html(task.name);
        var nameField = $('<td>').addClass('left-justified').html(viewLink);
        var editLink = $('<a>').addClass('edit-task').attr('href', taskUrl).html('Edit');
        var editField = $('<td>').addClass('left-justified').html(editLink);

        viewLink.click(viewTask);
        editLink.click(editTask);

        dataRow.append(statusField);
        dataRow.append(nameField);
        dataRow.append(editField);

        tableBody.append(dataRow);
    }

    tableHeader.append(topRow);
    table.append(tableHeader);
    table.append(tableBody);
    taskListDiv.append(table);

    // edit form needs populating, too
    var stateRadioButtonGroup = $('div#task-state-group');

    stateRadioButtonGroup.empty();

    for (var index = 0, len = data.stateArray.length; index < len; index++) {
        var state = data.stateArray[index];

        var stateButton = $('<input>')
            .addClass('task-state')
            .attr('id', 'task-state-' + state.id)
            .attr('type', 'radio')
            .attr('name', 'task-state')
            .val(state.id);
        var stateLabel = $('<label>')
            .addClass('btn btn-default task-state-label')
            .attr('id', 'task-state-label-' + state.id)
            .html(stateButton)
            .append(state.name);

        stateRadioButtonGroup.append(stateLabel);
    }

    var taskListSelect = $('#task-list');
    
    taskListSelect.empty();

    for (var index = 0, len = data.taskListArray.length; index < len; index++) {
        var taskList = data.taskListArray[index];

        var taskListOption = $('<option>')
            .addClass('task-list')
            .attr('id', 'task-list-' + taskList.id)
            .val(taskList.id)
            .html(taskList.name);

        taskListSelect.append(taskListOption);
    }
}

var taskListInit = function() {
    $.getJSON(taskListUrl, parseTaskListData);

    $('a.create-task').click(createTask);
    $('form#edit-task-form').submit(editFormSubmit);
};

$(document).ready(function() {
    taskListInit();
});
